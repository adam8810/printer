<?php
namespace Printer;

class Printer {
    public static function pre($data) {
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }
    
    public static function fixed($data) {
        echo '<div style="background:#FFF;border-top: 3px solid #333;bottom:0;left:0;height:400px;overflow-y:scroll;position:fixed;width:100%;z-index:9999">';
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        echo '</div>';
    }
}